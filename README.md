In this repository are the scripts and resources used by the Cental team in their participation in the TSAR Shared Task. 

# Abstract
Lexical simplification is the task of substituting a difficult word with a simpler equivalent for a target audience. This is currently commonly done by modeling lexical complexity on a continuous scale to identify simpler alternatives to difficult words. In the TSAR shared task, the organizers call for systems capable of generating substitutions in a zero-shot-task context, for English, Spanish and Portuguese. In this paper, we present the solution we (the \textsc{cental} team) proposed for the task. We explore the ability of BERT-like models to generate substitution words by masking the difficult word. To do so, we investigate various context enhancement strategies, that we combined into an ensemble method. We also explore different substitution ranking methods. We report on a post-submission analysis of the results and present our insights for potential improvements.

For more details, you can see the [poster](https://gitlab.com/Cental-FR/cental-tsar2022/-/blob/main/Poster.pdf) and the [paper](https://gitlab.com/Cental-FR/cental-tsar2022/-/blob/main/Paper.pdf). 


import glob
import pandas as pd
files = glob.glob("official_results/score/*")


method_labels = {"score_qe_tsar2022final_en_bert-large-uncased_m1_lmbert-large-uncased_probs.txt":"QE_{Bert_{large} M1}",
		"score_qe_tsar2022final_en_bert-large-uncased_m2_lmbert-large-uncased_probs.txt":"QE_{Bert_{large} M2}",
		"score_qe_tsar2022final_en_roberta-large_m1_lmroberta-large_probs.txt":"QE_{Roberta_{large} M1}",
		"score_qe_tsar2022final_en_roberta-large_m2_lmroberta-large_probs.txt":"QE_{Roberta_{large} M2}",
		"score_score_double_tsar2022final_en_bert-base-uncased_sepFalse.txt":"Repeat_{EN Base uncased}",
		"score_score_double_tsar2022final_en_bert-large-uncased_sepFalse.txt":"Repeat_{EN Large uncased}",
		"score_score_double_tsar2022final_en_roberta-base_sepFalse.txt":"Repeat_{EN Robert Base}",
		"score_score_double_tsar2022final_en_roberta-large_sepFalse.txt":"Repeat_{EN Roberta Large}",

		"score_tsar2022final_pagasus_8_8_en.txt":"Paraphrase_{EN}",

		"score_score_double_tsar2022final_es_dccuchile_bert-base-spanish-wwm-cased_sepFalse.txt":"Repeat_{ES cased}",
		"score_score_double_tsar2022final_es_dccuchile_bert-base-spanish-wwm-uncased_sepFalse.txt":"Repeat_{ES uncased}",
		"score_qe_tsar2022final_es_dccuchile_bert-base-spanish-wwm-cased_m1_lmdccuchile_bert-base-spanish-wwm-cased_probs.txt":"QE_{Bert M1}",
		"score_qe_tsar2022final_es_dccuchile_bert-base-spanish-wwm-cased_m2_lmdccuchile_bert-base-spanish-wwm-cased_probs.txt":"QE_{Bert M2}",


		"score_qe_tsar2022final_pt_neuralmind_bert-large-portuguese-cased_m1_lmneuralmind_bert-large-portuguese-cased_probs.txt":"QE_{Bert M1}",
		"score_qe_tsar2022final_pt_neuralmind_bert-large-portuguese-cased_m2_lmneuralmind_bert-large-portuguese-cased_probs.txt":"QE_{Bert M2}",
		"score_score_double_tsar2022final_pt_neuralmind_bert-base-portuguese-cased_sepFalse.txt":"Repeat_{PT Base}",
		"score_score_double_tsar2022final_pt_neuralmind_bert-large-portuguese-cased_sepFalse.txt":"Repeat_{PT Large}",


		"score_dico_tsar2022_en.tsv":"Dictionary_{EN}",
		"score_dico_tsar2022_es.tsv":"Dictionary_{ES}",
		"score_dico_tsar2022_pt.tsv":"Dictionary_{PT}",
}



evals = ["MAP@1/Potential@1/Precision@1", "MAP@3", "MAP@5", "MAP@10", "Potential@3", "Potential@5", "Potential@10", "Accuracy@1@top_gold_1", "Accuracy@2@top_gold_1", "Accuracy@3@top_gold_1"]
results = {}
for file in files:
	method_label = method_labels[file.split("/")[-1]]
	results[method_label] = {}
	with open(file) as input_file:
		for ln in input_file:
			if " = " in ln: 
				score, val = ln.split(" = ")
				if score in evals:
					val = float(val)
					results[method_label][score] = val


df = pd.DataFrame.from_dict(results).transpose()
print(df.to_latex())
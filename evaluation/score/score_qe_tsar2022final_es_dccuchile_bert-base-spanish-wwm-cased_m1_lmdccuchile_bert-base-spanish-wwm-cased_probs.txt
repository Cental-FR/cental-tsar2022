=========   EVALUATION config.=========
GOLD file = gold/tsar2022_es_test_gold.tsv
PREDICTION LABELS file = official_results/components/qe_tsar2022final_es_dccuchile_bert-base-spanish-wwm-cased_m1_lmdccuchile_bert-base-spanish-wwm-cased_probs.txt
OUTPUT file = official_results/score/score_qe_tsar2022final_es_dccuchile_bert-base-spanish-wwm-cased_m1_lmdccuchile_bert-base-spanish-wwm-cased_probs.txt
===============   RESULTS  =============
MAP@1/Potential@1/Precision@1 = 0.3831

MAP@3 = 0.2395
MAP@5 = 0.1752
MAP@10 = 0.1054

Potential@3 = 0.6114
Potential@5 = 0.6847
Potential@10 = 0.7717

Accuracy@1@top_gold_1 = 0.182
Accuracy@2@top_gold_1 = 0.2934
Accuracy@3@top_gold_1 = 0.3206

________________________________

=========   EVALUATION config.=========
GOLD file = gold/tsar2022_en_test_gold.tsv
PREDICTION LABELS file = official_results/components/qe_tsar2022final_en_roberta-large_m1_lmroberta-large_probs.txt
OUTPUT file = official_results/score/score_qe_tsar2022final_en_roberta-large_m1_lmroberta-large_probs.txt
===============   RESULTS  =============
MAP@1/Potential@1/Precision@1 = 0.3109

MAP@3 = 0.2397
MAP@5 = 0.1867
MAP@10 = 0.1208

Potential@3 = 0.5898
Potential@5 = 0.7345
Potential@10 = 0.8632

Accuracy@1@top_gold_1 = 0.1179
Accuracy@2@top_gold_1 = 0.2091
Accuracy@3@top_gold_1 = 0.2868

________________________________

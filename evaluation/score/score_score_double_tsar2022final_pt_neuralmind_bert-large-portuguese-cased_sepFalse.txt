=========   EVALUATION config.=========
GOLD file = gold/tsar2022_pt_test_gold.tsv
PREDICTION LABELS file = official_results/components/score_double_tsar2022final_pt_neuralmind_bert-large-portuguese-cased_sepFalse.txt
OUTPUT file = official_results/score/score_score_double_tsar2022final_pt_neuralmind_bert-large-portuguese-cased_sepFalse.txt
===============   RESULTS  =============
MAP@1/Potential@1/Precision@1 = 0.4705

MAP@3 = 0.2843
MAP@5 = 0.1984
MAP@10 = 0.1158

Potential@3 = 0.7032
Potential@5 = 0.7807
Potential@10 = 0.8395

Accuracy@1@top_gold_1 = 0.2513
Accuracy@2@top_gold_1 = 0.3689
Accuracy@3@top_gold_1 = 0.4144

________________________________

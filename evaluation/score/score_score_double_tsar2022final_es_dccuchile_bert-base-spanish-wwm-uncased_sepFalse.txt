=========   EVALUATION config.=========
GOLD file = gold/tsar2022_es_test_gold.tsv
PREDICTION LABELS file = official_results/components/score_double_tsar2022final_es_dccuchile_bert-base-spanish-wwm-uncased_sepFalse.txt
OUTPUT file = official_results/score/score_score_double_tsar2022final_es_dccuchile_bert-base-spanish-wwm-uncased_sepFalse.txt
===============   RESULTS  =============
MAP@1/Potential@1/Precision@1 = 0.2989

MAP@3 = 0.184
MAP@5 = 0.1298
MAP@10 = 0.0744

Potential@3 = 0.4809
Potential@5 = 0.5489
Potential@10 = 0.625

Accuracy@1@top_gold_1 = 0.1413
Accuracy@2@top_gold_1 = 0.2092
Accuracy@3@top_gold_1 = 0.2364

________________________________

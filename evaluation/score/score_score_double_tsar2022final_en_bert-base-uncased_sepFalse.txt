=========   EVALUATION config.=========
GOLD file = gold/tsar2022_en_test_gold.tsv
PREDICTION LABELS file = official_results/components/score_double_tsar2022final_en_bert-base-uncased_sepFalse.txt
OUTPUT file = official_results/score/score_score_double_tsar2022final_en_bert-base-uncased_sepFalse.txt
===============   RESULTS  =============
MAP@1/Potential@1/Precision@1 = 0.4959

MAP@3 = 0.3296
MAP@5 = 0.2496
MAP@10 = 0.1587

Potential@3 = 0.7479
Potential@5 = 0.8525
Potential@10 = 0.9276

Accuracy@1@top_gold_1 = 0.2627
Accuracy@2@top_gold_1 = 0.3833
Accuracy@3@top_gold_1 = 0.4611

________________________________
